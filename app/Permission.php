<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    public function roles(){
        return $this->belongsToMany(\App\Role::class, 'role_persmissions');
    }

    public function users(){
        return $this->belongsToMany(\App\User::class, 'user_permissions');
    }
}
