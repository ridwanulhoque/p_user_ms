<?php

use App\Permission;

trait HasPermissionsTrait
{


    public function getAllPermissions($permissions){
        return Permission::whereIn('slug', $permissions)->get();
    }


    public function permissions(){
        return $this->belongsToMany(\App\Permission::class, 'user_permissions');
    }

    public function givePermissionTo($permissions){ 

        if($permissions === null){
            return $this;
        }

        $permissions = $this->getAllPermissions($permissions);

        $this->permissions()->saveMany($permissions);

        return $this;
    }

    public function withdrawPermissionsTo($permissions){
        $permissions = $this->getAllPermissions($permissions);

        $this->permissions()->detach($permissions);

        return $this;
    }

    public function refreshPermissions($permissions){
        $this->permissions()->detach($permissions);

        return $this->givePermissionTo($permissions);
    }


    public function roles(){
        return $this->belongsToMany(\App\Role::class, 'user_roles');
    }

    public function hasPermissionThroughRole($permission){

        foreach($permission->roles as $role){
            if($this->roles->contains($role)){
                return true;
            }
        }

        return false;
    }

    public function hasPermission($permission){
        return (bool) $this->permissions->where('slug', $permission->slug)->count();
    }

    public function hasPermissionTo($permission){
        return $this->permissions()->hasPermissionThroughRole($permission) || $this->hasPermission($permission);
    }

    public function hasRole($roles){
        foreach($roles as $role){
            if($this->roles->contains('slug', $role)){
                return true;
            }
        }

        return false;
    }

}
